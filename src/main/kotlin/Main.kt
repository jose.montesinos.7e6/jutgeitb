import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import java.io.File
open class usuari(_nom:String, _cognom:String){
    var nom:String = _nom
    var cognom:String = _cognom
}

class alumne(_nom:String, _cognom:String, _registre:MutableList<Boolean>, _puntuacio:Double):usuari(_nom,_cognom){
    var registre:MutableList<Boolean> = _registre
    var puntuacio = _puntuacio
}

class profesor(_nom:String, _cognom:String, _contrasenya:String):usuari(_nom,_cognom){
    var contrasenya:String = _contrasenya
}




@Serializable
class Problema(
    var id: Int,
    var enunciat: String,
    var entradaPublica: String,
    var sortidaPublica: String,
    var entradaPrivada: String,
    var sortidaPrivada: String,
    var resolt: Boolean

){
    /*
    var prueva:Int = 1
    var a = listOf(1,2,3,4)

    init {
        for(x in a){
            println(x)
        }
    }*/
    fun test(i:Int):String{
        var file = File("Problemes/problema$i.json")
        var fileData = file.readText()
        val json = Json.decodeFromString<Problema>(fileData)

        println(this.enunciat)
        return this.enunciat
    }
}
fun enunciats(id:Int):String{
    var file = File("Problemes/problema$id.json")
    var fileData = file.readText()
    val json = Json.decodeFromString<Problema>(fileData)

    json.test()

    val enunciatProblema = json.enunciat
    return enunciatProblema
}
fun main() {

    //println(enunciats(1))
    enunciats(1)


}